### Chocoauth

Chocoauth is a small project that allows me to easily manage users. I use it to manage Matrix users (they log in 
thanks to openid provided by oauth2_provider) and OpenSMTPD/Dovecot users (the user password are store using bcrypt
which means that the database used by Django can also be used by those two software).

#### Configuration

##### RSA KEY
You need to generate a RSA key using the following command
```buildoutcfg
openssl genrsa -out oidc.key 4096
```

Then you need to inject it into the `OIDC_RSA_PRIVATE_KEY` environment variable.

#### Removing old tokens
You also need to setup a cronjob to clear old token with the command `python manage.py cleartokens`