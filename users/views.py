from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import FormView
from django.views.generic import TemplateView

from .models import User, Profile
from .forms import UserUpdateForm


class UserDetailView(LoginRequiredMixin, TemplateView):
    template_name = "users/profile.html"

class UserUpdateView(LoginRequiredMixin, FormView):

    template_name = "users/user_update.html"
    form_class = UserUpdateForm
    success_url = '/'

    def get_initial(self):
        initial = super().get_initial()
        user = self.request.user
        initial["first_name"] = user.first_name
        initial["last_name"] = user.last_name
        initial["email"] = user.email
        initial["display_name"] = user.profile.display_name
        return initial

    def form_valid(self,form):
        form_clean = form.cleaned_data
        user = User.objects.get(pk=self.request.user.pk)
        profile = Profile.objects.get(user=user)
        user.email = form_clean["email"]
        user.first_name = form_clean["first_name"]
        user.last_name = form_clean["last_name"]
        user.save()
        profile.display_name = form_clean["display_name"]
        if 'avatar' in self.request.FILES.keys():
            profile.avatar = self.request.FILES['avatar']
        profile.save()
        return super().form_valid(form)
