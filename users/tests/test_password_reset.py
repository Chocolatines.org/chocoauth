from django.test import TestCase, Client
from django.core import mail
from users.models import User

class PasswordResetViewCase(TestCase):

    def setUp(self):
        self.username = "john"
        self.email = "test@test.fr"
        self.password = "password1"
        User.objects.create(username=self.username,
                            email=self.email)
        user = User.objects.get(username=self.username)
        user.set_password(self.password)
        user.save()

    def test_passsord_reset_from(self):
        c = Client()
        response = c.post('/password_reset/', {"email": self.email}, follow=True)
        self.assertEqual(("/password_reset/done/", 302), response.redirect_chain[0])
        self.assertEqual(200, response.status_code)
        self.assertEqual(1, len(mail.outbox))
        self.assertTrue("you requested a password reset" in mail.outbox[0].body)

