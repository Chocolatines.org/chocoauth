from django.test import TestCase, Client
from users.models import User

class LoginViewCase(TestCase):

    def setUp(self):
        User.objects.create(username="username",
                            first_name="John",
                            last_name="Lenon")
        user = User.objects.get(username="username")
        user.set_password("password1")
        user.save()

    def test_profile_view_redirect(self):
        c = Client()
        response = c.get('/', follow=True)
        # should redirect to login (/login/) with 302 status code
        self.assertEqual(("/login/?next=/", 302), response.redirect_chain[0])
        self.assertEqual(200, response.status_code)

    def test_profile_view_redirect(self):
        c = Client()
        c.login(username="username", password="password1")
        response = c.get('/', follow=True)
        self.assertEqual(200, response.status_code)