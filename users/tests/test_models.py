from django.test import TestCase
from users.models import User, Profile

# Create your tests here.

class UserTestCase(TestCase):
    def setUp(self):
        User.objects.create(username="username",
                            first_name="John",
                            last_name="Lenon")

    def test_profile_creation(self):
        # Check if a profile is created when creating a new user
        # and assigned the proper display name
        user = User.objects.get(username="username")
        profile = Profile.objects.get(user=user)
        self.assertEqual("username", profile.get_display_name())

    def test_profile_display_name(self):
        # Check if the display method works properly
        user = User.objects.get(username="username")
        profile = Profile.objects.get(user=user)
        profile.display_name = "John"
        self.assertEqual("John", profile.get_display_name())

