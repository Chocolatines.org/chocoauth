from django.test import TestCase, Client
from users.models import User

class LoginViewCase(TestCase):

    def setUp(self):
        User.objects.create(username="username",
                            first_name="John",
                            last_name="Lenon")
        user = User.objects.get(username="username")
        user.set_password("password1")
        user.save()

    def test_login_view_redirect(self):
        c = Client()
        response = c.post('/login/',
                          {"username": "username", "password": "password1"},
                          follow=True)
        # should redirect to profile (/) with 302 status code
        self.assertEqual(("/", 302), response.redirect_chain[0])
        self.assertEqual(200, response.status_code)

    def test_login_view_fail(self):
        c = Client()
        response = c.post('/login/',
                          {"username": "username", "password": "password"},
                          follow=True)
        self.assertEqual(200, response.status_code)
