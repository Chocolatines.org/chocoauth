from django.test import TestCase, Client
from users.models import User, Profile

class LoginViewCase(TestCase):

    def setUp(self):
        User.objects.create(username="username",
                            first_name="John",
                            last_name="Lenon")
        user = User.objects.get(username="username")
        user.set_password("password1")
        user.save()

    def test_profile_view_login_redirect(self):
        c = Client()
        response = c.get('/profile/update/', follow=True)
        # should redirect to login (/login/) with 302 status code
        self.assertEqual(("/login/?next=/profile/update/", 302), response.redirect_chain[0])
        self.assertEqual(200, response.status_code)

    def test_profile_view_display_name_update(self):
        c = Client()
        c.login(username="username", password="password1")
        response = c.post('/profile/update/',
                          {"display_name": "coucou", "email": "test@coucou.fr"},
                          follow=True)
        user = User.objects.get(username="username")
        profile = Profile.objects.get(user=user)
        self.assertEqual("coucou", profile.get_display_name())
        self.assertEqual(("/", 302), response.redirect_chain[0])
        self.assertEqual(200, response.status_code)

    def test_profile_view_fail(self):
        c = Client()
        c.login(username="username", password="password1")
        response = c.post('/profile/update/',
                          {"display_name": "coucou"})
        self.assertEqual(200, response.status_code)
        # email is missing
        self.assertTrue('This field is required' in str(response.content))