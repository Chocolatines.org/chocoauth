from django.db import models
from django.contrib.auth.models import AbstractUser
from django.urls import reverse

def user_avatar_path(instance, filename):
    return "avatar/{0}/{1}".format(instance.user.username, filename)


class User(AbstractUser):

    email = models.EmailField('email address', unique=True, blank=False, null=False)
    
    def get_absolute_url(self):
        return reverse('profile', args=(self.pk,))

    def save(self, *args, **kwargs):
        super(User, self).save(*args, **kwargs)
        if Profile.objects.filter(user=self).count() == 0:
            profile = Profile(user=self, display_name=self.username)
            profile.save()


class Profile(models.Model):

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    display_name = models.CharField(max_length=60)
    avatar = models.ImageField(upload_to=user_avatar_path, null=True, blank=True)

    def __str__(self):
        return "{} profile".format(self.user.username)

    def get_display_name(self):
        if self.display_name is None or self.display_name == "":
            return self.user.username
        return self.display_name
