from django import forms


class UserUpdateForm(forms.Form):
    first_name = forms.CharField(required=False)
    last_name = forms.CharField(required=False)
    display_name = forms.CharField(required=False)
    email = forms.EmailField()
    avatar = forms.ImageField(required=False)
